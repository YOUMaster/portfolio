var my_works = [
	{
		img_link: 'img/scr/spform.png',
		name: 'SPForm',
		link: 'spform.com',
		description: 'Wordpress, Верстка, phpBB'
	},
	{
		img_link: 'img/scr/centralexpert.png',
		name: 'Central Expert',
		link: 'central-expert.ru',
		description: 'Wordpress, Верстка'
	},
	{
		img_link: 'img/scr/formingpack.png',
		name: 'Forming Pack',
		link: 'formingpack.com.ua',
		description: 'Wordpress, Верстка'
	},
	{
		img_link: 'img/scr/glassag.png',
		name: 'Глассажъ',
		link: 'glassag.ru',
		description: 'Wordpress, Верстка'
	},
	{
		img_link: 'img/scr/modobot.png',
		name: 'Modobot',
		link: 'modobot.com',
		description: 'Верстка'
	},
	{
		img_link: 'img/scr/powersales.png',
		name: 'Power Sales',
		link: 'power-sales.ru',
		description: 'Wordpress, Верстка'
	},
	{
		img_link: 'img/scr/doctorovpro.png',
		name: 'Doctorov Pro',
		link: 'doctorov.pro',
		description: 'Верстка'
	},
	{
		img_link: 'img/scr/resurse-kp.jpg',
		name: 'Ресурскомплект',
		link: 'resurse-kp.com',
		description: 'Wordpress, Верстка'
	},
	{
		img_link: 'img/scr/rsa.jpg',
		name: 'СТО «Олимп Сервис»',
		link: 'rsa.kiev.ua',
		description: 'Wordpress, Bootstrap, Редизайн'
	},
	{
		img_link: 'img/scr/mbronnaya.png',
		name: 'Театр на Малой Бронной',
		link: 'www.mbronnaya.ru',
		description: 'Верстка'
	},
	{
		img_link: 'img/scr/abbholod.png',
		name: 'ABB Holod',
		link: 'abbholod.ru',
		description: 'Wordpress, Верстка'
	},
	{
		img_link: 'img/scr/chocolate.jpg',
		name: 'Chocolate Holiday',
		link: 'chocolate-holiday.com.ua',
		description: 'Wordpress, Верстка'
	}
];

var Works = React.createClass({
	render: function() {
		var data = this.props.data;
		var divStyle = {
			// display: 'none',
			// transform: 'translateY(0px)',
		};
		var worksTemplate = data.map(function(item, index) {
			return (
				<div key={index} className={'col s12 m6 l4'}>
					<div className={'card hoverable'}> 
						<div className={'card-image'}>
							<img alt='img' className={'activator'} src={item.img_link} />
						</div>
						<div className={'card-content'}>
							<span classNames={'card-title activator grey-text text-darken-4'}>{item.name}<i className={'material-icons right'}>more_vert</i></span>
							<p><i title={item.link}>{item.link}</i></p>
						</div>
						<div className={'card-reveal'} style={divStyle}>
							<span className={'card-title ' + 'grey-text ' + 'text-darken-4'}>
								Описание работы:<i className={'material-icons right'}>close</i><span className={'card-more'}>{item.description}</span>
							</span> 
						</div>
					</div>
				</div>
			)
		})
		return (
			<div className={'col s11 mycontainer'}>
				{worksTemplate}
			</div>
		);
	}
});

var App = React.createClass({
	render: function() {
		return (
			<div className={'app row'}>
				<h2 className={'header center-align'} id='portfolio'>ПОРТФОЛИО</h2>
				<h5 className={'center-align'}>Работы</h5>
				<div className={'divider'}></div>
				<Works data={my_works} /> {/*добавили свойство data */}
			</div>
		);
	}
});

ReactDOM.render(
	<App />,
	document.getElementById('works')
);