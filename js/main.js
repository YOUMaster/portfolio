$(document).ready(function() {
	$('.parallax').parallax();
	$(".button-collapse").sideNav();
	$(window).scroll(function(){
		if ($(window).scrollTop() > 768) {
			$("header nav a[href=#top]").removeClass("hide");
		}
		else{
			$("header nav a[href=#top]").addClass("hide");
		}
	});
});
$(window).load(function(){
	$("a[href*='#']").mPageScroll2id({highlightSelector:"a[href*='#']"});
});

$(document).on('click.card', '.card', function (e) {
	if ($(this).find('> .card-reveal').length) {
		if ($(e.target).is($('.card-reveal .card-title')) || $(e.target).is($('.card-reveal .card-title i'))) {
			// Make Reveal animate down and display none
			$(this).find('.card-reveal').velocity(
				{translateY: 0}, {
					duration: 225,
					queue: false,
					easing: 'easeInOutQuad',
					complete: function() { $(this).css({ display: 'none'}); }
				}
			);
		}
		else if ($(e.target).is($('.card .card-content>span')) ||
						 $(e.target).is($('.card .card-content>span i')) ) {
			$(e.target).closest('.card').css('overflow', 'hidden');
			$(this).find('.card-reveal').css({ display: 'block'}).velocity("stop", false).velocity({translateY: '-100%'}, {duration: 300, queue: false, easing: 'easeInOutQuad'});
		}
	}
});

setTimeout(function() {
	$("i[title]").each(function(){
		$(this).replaceWith("<a href='http://" + $(this).attr("title") + "'>" +$(this).html() + "</a>");
	});
	$(document).on('each', 'i[title]', function() {
		$(this).replaceWith("<a href='http://" + $(this).attr("title") + "'>" +$(this).html() + "</a>");
	});
	$("a[href^=http]").attr("target", "_blank");  
}, 1000);