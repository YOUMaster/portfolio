<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>Портфолио</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="icon" type="image/png" href="favicon.png">
  </head>
  <body>

    <header>
      <div class="navbar-fixed">
        <nav>
          <div class="nav-wrapper">
            <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
              <li><a href="#top" class="hide">В начало</a></li>
              <li><a href="#portfolio">Портфолио</a></li>
              <li><a href="#contacts">Контакты</a></li>
            </ul>
            <ul class="side-nav" id="mobile-menu">
              <li><a href="#top" class="hide">В начало</a></li>
              <li><a href="#portfolio">Портфолио</a></li>
              <li><a href="#contacts">Контакты</a></li>
            </ul>
          </div>
        </nav>
      </div>
    </header>
    <main>
      <section class="parallax-container valign-wrapper" style="background-color: rgba(234,69,75, 0.1);" id="top">
        <div class="parallax"><img alt="img" src="img/parallax1.jpg"></div>
        <h1 class="valign center mynameis"><span class="name">Николай Гнатовский</span><span class="info animated fadeInUp">Wordpress / Front-End разработчик</span></h1>
      </section>

      <section class="section white">
        <div class="row">
          <div class="col s11 mycontainer">
            <h2 class="header center-align" id="portfolio">ПОРТФОЛИО</h2>
            <h5 class="center-align">Мои последние работы</h5>
            <div class="divider"></div>






            <div class="col s12 m6 l4">
              <div class="card hoverable"> 
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/spform.png">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">SPForm<i class="material-icons right">more_vert</i></span>
                  <p><i title="spform.com">spform.com</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i>
                  <span class="card-more">Wordpress, Верстка, phpBB</span>
                  </span>
                </div>
              </div>
            </div>






            <div class="col s12 m6 l4">
              <div class="card hoverable"> 
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/twist.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Школа вождения<i class="material-icons right">more_vert</i></span>
                  <p><i title="drivetwist.kiev.ua/">drivetwist.kiev.ua</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i>
                  <span class="card-more">Wordpress, Верстка</span>
                  </span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/webrotor.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Вебротор<i class="material-icons right">more_vert</i></span>
                  <p><i title="webrotor.guru">webrotor.guru</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i>
                    <span class="card-more">Wordpress, Верстка</span>
                  </span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/resurse-kp.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Ресурскомплект<i class="material-icons right">more_vert</i></span>
                  <p><i title="resurse-kp.com">resurse-kp.com</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/rsa.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">СТО «Олимп Сервис»<i class="material-icons right">more_vert</i></span>
                  <p><i title="rsa.kiev.ua">rsa.kiev.ua</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Bootstrap, Редизайн</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/bulgaria.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Болгария<i class="material-icons right">more_vert</i></span>
                  <p><i title="bulgariapass.com">bulgariapass.com</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/radiotest.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Радиотест<i class="material-icons right">more_vert</i></span>
                  <p><i title="radiotest.ru">radiotest.ru</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, WooCommerce, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable"> 
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/chocolate.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Chocolate Holiday<i class="material-icons right">more_vert</i></span>
                  <p><i title="chocolate-holiday.com.ua">chocolate-holiday.com.ua</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/kuhonshik.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Кухонщик<i class="material-icons right">more_vert</i></span>
                  <p><i title="kuhonshik.com">kuhonshik.com</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, WooCommerce, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/sea.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Моряки<i class="material-icons right">more_vert</i></span>
                  <p><i title="time4job.com.ua">time4job.com.ua</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Bootstrap, Редизайн</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable"> 
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/iqpoliv.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">IQ Полив<i class="material-icons right">more_vert</i></span>
                  <p><i title="lp.iqpoliv.ru">lp.iqpoliv.ru</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Верстка, отправка html-писем</span></span>
                </div>
              </div>
            </div>
            
            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/phar20-min.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Phar20<i class="material-icons right">more_vert</i></span>
                  <p><i title="phar20.adr.com.ua">phar20.adr.com.ua</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/specstyle.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Спецстиль<i class="material-icons right">more_vert</i></span>
                  <p><i title="spetsstil.com.ua">spetsstil.com.ua</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, WooCommerce, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/ls-group-min.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">LS Group<i class="material-icons right">more_vert</i></span>
                  <p><i title="ls-group.com.ua">ls-group.com.ua</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/artist-min.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">Сайт художника<i class="material-icons right">more_vert</i></span>
                  <p><i title="artist.hol.es">artist.hol.es</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Верстка</span></span>
                </div>
              </div>
            </div>

            <div class="col s12 m6 l4">
              <div class="card hoverable">
                <div class="card-image">
                  <img alt="img" class="activator" src="img/scr/azs.jpg">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">АЗС<i class="material-icons right">more_vert</i></span>
                  <p><i title="http://polidecisions.com">polidecisions.com</i></p>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Описание работы:<i class="material-icons right">close</i><span class="card-more">Wordpress, Верстка</span></span>
                </div>
              </div>
            </div>


          </div>
        </div>
      </section>
      <section class="section white">
        <h3 class="header center-align" id="contacts">КОНТАКТЫ</h3>

        <div class="row">
          <div class="col s11 mycontainer">

            <div class="col s12 m6 mydata">

              <div class="col s12">
                <div class="col s12 m1 center-align">
                  <i class="blue-text fa fa-envelope-o" aria-hidden="true"></i>
                </div>
                <div class="col s12 m11">
                  <a href="mailto:nikolay@hnatovskyi@gmail.com">nikolay@hnatovskyi@gmail.com</a>
                </div>
              </div>


              <div class="col s12">
                <div class="col s12 m1 center-align">
                  <i class="blue-text fa fa-skype" aria-hidden="true"></i>
                </div>
                <div class="col s12 m11">
                  <a href="skype:kolya634?call">kolya634</a>
                </div>
              </div>

              <div class="col s12">
                <div class="col s12 m1 center-align">
                  <i class="blue-text fa fa-mobile" aria-hidden="true"></i>
                </div>
                <div class="col s12 m11">
                   <a href="tel:+38(096)115-74-31">+38(096)115-74-31</a>  
                </div>
              </div>

              <div class="col s12">
                <div class="col s12 m1 center-align">
                  <i class="blue-text fa fa-mobile" aria-hidden="true"></i>
                </div>
                <div class="col s12 m11">
                   <a href="tel:+38(050)601-50-56">+38(050)601-50-56</a>
                </div>
              </div>

              <div class="col s12">
                <div class="col s12 m1 center-align">
                  <i class="blue-text fa fa-paper-plane" aria-hidden="true"></i>
                </div>
                <div class="col s12 m11">
                   +38(096)115-74-31
                </div>
              </div>

              <div class="col s12">
                <div class="col s12 m1 center-align">
                  <i class="blue-text fa fa-bitbucket" aria-hidden="true"></i>
                </div>
                <div class="col s12 m11">
                  <a href="http://bitbucket.org/YOUMaster">Bitbucket</a>
                </div>
              </div>

              <div class="col s12">
                <div class="col s12 m1 center-align">
                  <i class="blue-text fa fa-linkedin" aria-hidden="true"></i>
                </div>
                <div class="col s12 m11">
                  <a href="http://linkedin.com/in/nikolay-hnatovskyi">LinkedIn</a>
                </div>
              </div>

              <div class="col s12">
                <div class="col s12 m1 center-align">
                  <i class="blue-text fa fa-vk" aria-hidden="true"></i>
                </div>
                <div class="col s12 m11">
                  <a href="http://vk.com/id52044524">Vk</a>
                </div>
              </div>
            </div>
            <div class="col s12 m6">
              <form method="post" class="col s12" id="form_id">
                <div class="input-field col s12">
                  <i class="material-icons prefix">account_circle</i>
                  <input id="icon_prefix" type="text" name="your-name" class="validate" required>
                  <label for="icon_prefix">Ваше имя</label>
                </div>
                <div class="input-field col s12">
                  <i class="material-icons prefix">email</i>
                  <input id="email" type="email" name="email" class="validate">
                  <label for="email">Email</label>
                </div>
                <div class="input-field col s12">
                  <i class="material-icons prefix">phone</i>
                  <input id="icon_telephone" type="tel" name="tel" class="validate">
                  <label for="icon_telephone">Номер телефона</label>
                </div>
                <div class="input-field col s12">
                  <i class="material-icons prefix">mode_edit</i>
                  <textarea id="textarea1" name="message" class="materialize-textarea"></textarea>
                  <label for="textarea1">Сообщение</label>
                </div>
                <div class="col s12">
                  <div id="result_id" class="left"></div>
                  <button class="btn waves-effect waves-light right" type="submit" name="action" id="button">Отправить
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>

    </main>
    <footer class="page-footer">
      <nav>
        <ul class="right">
          <li><a href="#top">В начало</a></li>
          <li><a href="#portfolio">Портфолио</a></li>
          <li><a href="#contacts">Контакты</a></li>
        </ul>
      </nav>
      <div class="footer-copyright">
        <div class="row">
          <div class="offset-m1 col s12 m10">
            © 2016 Николай Гнатовский
            <a class="grey-text text-lighten-4 right" href="http://vk.com/id52044524"><i class="fa fa-vk" aria-hidden="true"></i></a>
            <a class="grey-text text-lighten-4 right" href="http://linkedin.com/in/nikolay-hnatovskyi"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <a class="grey-text text-lighten-4 right" href="http://bitbucket.org/YOUMaster"><i class="fa fa-bitbucket" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
      <!-- <div class="fixed-action-btn horizontal" style="bottom: 45px; right: 24px;">
        <a href="/ru" class="btn-floating btn-large purple">
          RU
        </a>
        <ul>
          <li><a href="/ua" class="btn-floating green darken-1"><i>UA</i></a></li>
          <li><a href="/en" class="btn-floating blue"><i>EN</i></a></li>
        </ul>
      </div> -->

      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script src="js/jquery.malihu.PageScroll2id.min.js"></script>
      <script src="js/jquery.hashchange.min.js"></script>
      <script src="js/main.js"></script>
      <script>
        // Аякс отправка
        function AjaxFormRequest(result_id,form_id,url) {
          jQuery.ajax({
            url:     url, //Адрес подгружаемой страницы
            type:     "POST", //Тип запроса
            dataType: "html", //Тип данных
            data: jQuery("#"+form_id).serialize(), 
            success: function(response) { //Если все нормально
              //document.getElementById(result_id).innerHTML = response;
              $("#result_id").text("Отправлено успешно!");
              document.getElementById(form_id).reset();
            },
            error: function(response) { //Если ошибка
              //document.getElementById(result_id).innerHTML = "Ошибка при отправке формы";
              $("#result_id").text("Ошибка при отправке формы");
            }
          });
        }
        $(document).ready(function(){
          $('#button').click(function(e){
            AjaxFormRequest('result_div_id', 'form_id', 'form.php');
            e.preventDefault(); 
          });
        });
      </script>
      <link type="text/css" rel="stylesheet" href="css/materialize.css">
      <link rel="stylesheet" type="text/css" href="style.css">
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    </footer>
  </body>
</html>